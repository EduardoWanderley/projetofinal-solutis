﻿using EstateAgency.Entities;
using System;
using System.Collections.Generic;

namespace EstateAgency.Interfaces.Repositories {
    public interface IRepositoryImmobile {
        void Add(Immobile request);

        void Update(Immobile request);

        IEnumerable<Immobile> List();

        IEnumerable<Immobile> ListByCep();

        Immobile FindById(Guid id);

        void Delete(Immobile immobile);
    }
}

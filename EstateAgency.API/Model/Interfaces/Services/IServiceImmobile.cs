﻿using EstateAgency.Arguments.Immobile;
using EstateAgency.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EstateAgency.Interfaces.Services {
    public interface IServiceImmobile {

        Task Add(ImmobileRequest request);

        Task Update(UpdateImmobileRequest request);

        IEnumerable<Immobile> List();

        IEnumerable<Immobile> ListByCep(string cep);

        Immobile FindById(Guid id);
      
        ImmobileResponse Delete(Guid id);
    }
}

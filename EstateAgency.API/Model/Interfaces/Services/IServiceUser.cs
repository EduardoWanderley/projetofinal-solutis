﻿using EstateAgency.Arguments.User;
using EstateAgency.Entities;
using System;
using System.Collections.Generic;

namespace EstateAgency.Interfaces.Services {
    public interface IServiceUser {

        void Add(AddUserRequest request);

        void Update(UpdateUserRequest request);

        IEnumerable<UserResponse> List();

        UserResponse FindById(Guid id);

        void Delete(Guid id);

    }
}

﻿using EstateAgency.Enums;

namespace EstateAgency.Arguments.Immobile {
    public class ImmobileRequest {
        public ImmobileRequest(
            EnumTypeImmobilecs typeImmobile, int quantityRooms, decimal price, long ownerPhone, string cep) 
            {
            TypeImmobile = typeImmobile;
            QuantityRooms = quantityRooms;
            Price = price;
            OwnerPhone = ownerPhone;
            Cep = cep;
        }

        public ImmobileRequest() {       
        }

        public EnumTypeImmobilecs TypeImmobile { get; set; }
        public int QuantityRooms { get; set; }
        public decimal Price { get; set; }
        public long OwnerPhone { get; set; }
        public string Cep { get; set; }
    }
}

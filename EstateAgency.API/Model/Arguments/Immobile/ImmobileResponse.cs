﻿using EstateAgency.Enums;
using System;

namespace EstateAgency.Arguments.Immobile {
    public class ImmobileResponse {

        public Guid Id { get; set; }
        public EnumTypeImmobilecs Type { get; set; }
        public string Message { get; set; }

        public ImmobileResponse()  : base() {
            
        }

        public static explicit operator ImmobileResponse(Entities.Immobile v) {
            return new ImmobileResponse() {
                Id = v.Id,
                Type = v.Type
            };
        }
    }
}

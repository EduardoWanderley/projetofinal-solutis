﻿using EstateAgency.Enums;
using Newtonsoft.Json;
using System;

namespace EstateAgency.Arguments.Immobile {
    public class UpdateImmobileRequest {
        public UpdateImmobileRequest(Guid id,EnumTypeUser typeUser, EnumTypeImmobilecs typeImmobile, int quantityRooms, decimal price, long ownerPhone, string cep) {
            Id = id;
            TypeUser = typeUser;
            TypeImmobile = typeImmobile;
            QuantityRooms = quantityRooms;
            Price = price;
            OwnerPhone = ownerPhone;
            Cep = cep;
        }

        public UpdateImmobileRequest() {
        }

        public Guid Id { get; set; }
        public EnumTypeUser TypeUser { get; set; }
        public EnumTypeImmobilecs TypeImmobile { get; set; }
        public int QuantityRooms { get; set; }
        public decimal Price { get; set; }
        public long OwnerPhone { get; set; }
        public string Cep { get; set; }

    }
}

﻿using EstateAgency.Enums;
using System;

namespace EstateAgency.Arguments.User {
    public class UpdateUserRequest {
        public UpdateUserRequest(Guid id, string userName, string password, EnumTypeUser typeUser) {
            Id = id;
            UserName = userName;
            Password = password;
            TypeUser = typeUser;
        }

        public UpdateUserRequest() {
        }

        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public EnumTypeUser TypeUser { get; set; }
 
    }
}

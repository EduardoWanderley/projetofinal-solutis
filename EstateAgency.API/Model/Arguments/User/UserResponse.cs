﻿using EstateAgency.Enums;
using System;

namespace EstateAgency.Arguments.User {
    public class UserResponse {
        public UserResponse() {
        }

        public UserResponse(Guid id, string userName, string typeUser) {
            Id = id;
            UserName = userName;
            TypeUser = typeUser;
        }

        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string TypeUser { get; set; }

        public static explicit operator UserResponse(Entities.User entidade) {
            return new UserResponse() {
                Id = entidade.Id,
                UserName = entidade.UserName,
                TypeUser = entidade.TypeUser.ToString()
            };
        }

        public override string ToString() {
            return $"Id : {Id}\nNome de usuário : {UserName}\nTipo de usuário : {TypeUser.ToString()}";
        }
    }
}

﻿using EstateAgency.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstateAgency.Arguments.User {
    public class AddUserRequest {
        public AddUserRequest(string userName, string password, EnumTypeUser typeUser) {
            UserName = userName;
            Password = password;
            TypeUser = typeUser;
        }
        public AddUserRequest() {
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public EnumTypeUser TypeUser { get; set; }

    }
}

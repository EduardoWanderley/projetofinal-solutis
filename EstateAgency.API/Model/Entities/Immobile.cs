﻿using EstateAgency.Enums;
using Newtonsoft.Json;
using prmToolkit.NotificationPattern;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EstateAgency.Entities {
    [Table("dbo.immobiles")]
    public class Immobile  : Notifiable {

        [Column("id")]
        public Guid Id { get; set; }
        [Column("immobile_type")]
        public EnumTypeImmobilecs Type { get; set; }
        [Column("quantity_rooms")]
        public int QuantityRooms { get; set; }
        [Column("[price")]
        public decimal Price { get; set; }
        [Column("owner_phone")]
        public long OwnerPhone { get; set; }
        [Column("cep")]
        [JsonProperty("cep")]
        public string Cep { get; set; }
        [Column("logradouro")]
        [JsonProperty("logradouro")]
        public string Logradouro { get; set; }
        [Column("complemento")]
        [JsonProperty("complemento")]
        public string Complemento { get; set; }
        [Column("bairro")]
        [JsonProperty("bairro")]
        public string Bairro { get; set; }
        [Column("localidade")]
        [JsonProperty("localidade")]
        public string Localidade { get; set; }
        [Column("uf")]
        [JsonProperty("uf")]
        public string Uf { get; set; }
        [Column("unidade")]
        [JsonProperty("unidade")]
        public string Unidade { get; set; }
        [Column("ibge")]
        [JsonProperty("ibge")]
        public string Ibge { get; set; }
        [Column("gia")]
        [JsonProperty("gia")]
        public string Gia { get; set; }

        public Immobile(EnumTypeImmobilecs type, int quantityRooms, decimal price, long ownerPhone, string cep) {
            Id = Guid.NewGuid();
            Type = type;
            QuantityRooms = quantityRooms;
            Price = price;
            OwnerPhone = ownerPhone;
            Cep = cep;

            new AddNotifications<Immobile>(this)
                .IfNullOrInvalidLength(x => x.Cep, 8,8, "Por favor, digite um cep válido.");
        }

        public Immobile() {
        }

        public override string ToString() {
            return $"\nTipo de imóvel : {Type}\nQuantidade de quartos : {QuantityRooms}\nPreço : {Price}\n" +
                $"Telefone do proprietário : {OwnerPhone}\nCep : {Cep}\nLogradouro : { Logradouro}\nComplemento : {Complemento}\n" +
                $"Bairro : {Bairro}\nLocalidade : {Localidade}\nUf : {Uf}\nUnidade : {Unidade}\nIbge : {Ibge}\nGia : {Gia}";
        }

    }
}

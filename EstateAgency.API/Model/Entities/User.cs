﻿using EstateAgency.Enums;
using prmToolkit.NotificationPattern;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EstateAgency.Entities {
    [Table("dbo.users")]
    public class User : Notifiable {

        [Column("Id")]
        public Guid Id { get; set; }
        [Column("user_name")]
        public string UserName { get; set; }
        [Column("password")]
        public string Password { get; set; }
        [Column("user_type")]
        public EnumTypeUser TypeUser { get; set; }

        public User(string userName, string password, EnumTypeUser typeUser) {
            Id = Guid.NewGuid();
            UserName = userName;
            Password = password;
            TypeUser = typeUser;

            new AddNotifications<User>(this)
                .IfNullOrInvalidLength(x => x.UserName,4,20,"O nome deve ter entre 6 e 15 caracteres.")
                .IfNullOrInvalidLength(x => x.Password,6,12,"A senha deve ter entre 6 e 12 caracteres.")
                .IfEnumInvalid(x => x.TypeUser,"Inválido. Por favor digite no formato de ENUM.");
                
        }

        public User() {
        }

    }
}
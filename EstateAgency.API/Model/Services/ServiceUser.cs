﻿using EstateAgency.Arguments.User;
using EstateAgency.Entities;
using EstateAgency.Interfaces.Repositories;
using EstateAgency.Interfaces.Services;
using prmToolkit.NotificationPattern;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EstateAgency.Services {
    public class ServiceUser : Notifiable , IServiceUser {

        private readonly IRepositoryUser _repositoryUser;

        public ServiceUser() {
        }

        public ServiceUser(IRepositoryUser repositoryUser) {
            _repositoryUser = repositoryUser;
        }
        
        public void Add(AddUserRequest request) {
            

            User user = new User(request.UserName,request.Password,request.TypeUser);

            AddNotifications(user);

            if (this.IsInvalid()) {
                return;
            }

            _repositoryUser.Add(user);
        }

        public void Delete(Guid id) {
            var user = _repositoryUser.FindById(id);

            if (user == null) {
                AddNotification("id", "Dados não encontrados.");
                return;
            }

            _repositoryUser.Delete(user);
        }

        public UserResponse FindById(Guid id) {           
            var user = _repositoryUser.FindById(id);

            if(user == null) {
                AddNotification("id", "Dados não encontrados.");
                return null;
            }

            return (UserResponse)user;
        }

        public IEnumerable<UserResponse> List() {
            return _repositoryUser.List().ToList().Select(user => (UserResponse) user).ToList();
        }

        public void Update(UpdateUserRequest request) {
            var user = _repositoryUser.FindById(request.Id);

            if(user == null) {
                AddNotification("Id", "Dados não encontrados.");
            }

            user.UserName = request.UserName;
            user.Password = request.Password;
            user.TypeUser = request.TypeUser;

            AddNotifications(user);

            if (this.IsInvalid()) {
                return;
            }

            _repositoryUser.Update(user);
        }

    }
}

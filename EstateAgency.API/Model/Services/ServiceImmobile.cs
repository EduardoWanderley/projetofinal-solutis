﻿using EstateAgency.Arguments.Immobile;
using EstateAgency.Entities;
using EstateAgency.Interfaces.Repositories;
using EstateAgency.Interfaces.Services;
using prmToolkit.NotificationPattern;
using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EstateAgency.Services {
    public class ServiceImmobile : Notifiable, IServiceImmobile {

        private readonly IRepositoryImmobile _repositoryImmobile;
        public ServiceImmobile() {
        }
        public ServiceImmobile(IRepositoryImmobile repository) {
            _repositoryImmobile = repository;
        }

        public async Task Add(ImmobileRequest request) {

            if (request == null) {
                return;
            }

            var cepClient = RestService.For<ICepApiService>("http://viacep.com.br");


            Immobile immobile = new Immobile(request.TypeImmobile, request.QuantityRooms, request.Price, request.OwnerPhone, request.Cep);

            AddNotifications(immobile);

            var adress = await cepClient.GetAdressAssync(immobile.Cep);

            immobile.Logradouro = adress.Logradouro;
            immobile.Complemento = adress.Complemento;
            immobile.Bairro = adress.Bairro;
            immobile.Localidade = adress.Localidade;
            immobile.Uf = adress.Uf;
            immobile.Unidade = adress.Unidade;
            immobile.Ibge = adress.Ibge;
            immobile.Gia = adress.Gia;

            if (this.IsInvalid()) {
                return;
            }

            _repositoryImmobile.Add(immobile);

        }

        public ImmobileResponse Delete(Guid id) {
            var immobile = _repositoryImmobile.FindById(id);

            if (immobile == null) {
                AddNotification("id", "Dados não encontrados.");
                return null;
            }

            _repositoryImmobile.Delete(immobile);

            return (ImmobileResponse)immobile;

        }

        public Immobile FindById(Guid id) {

            var immobile = _repositoryImmobile.FindById(id);

            if (immobile == null) {
                AddNotification("id", "Dados não encontrados.");
                return null;
            }

            return immobile;
        }

        public IEnumerable<Immobile> List() {
            return _repositoryImmobile.List().ToList();
        }

        public IEnumerable<Immobile> ListByCep(string cep) {
            return _repositoryImmobile.ListByCep().ToList().Where(i => i.Cep == cep).ToList();
        }

        public async Task Update(UpdateImmobileRequest request) {

            if (request == null) {
                //logica de validacao
            }

            Immobile immobile = _repositoryImmobile.FindById(request.Id);

            var cepClient = RestService.For<ICepApiService>("http://viacep.com.br");

            immobile.Type = request.TypeImmobile;
            immobile.OwnerPhone = request.OwnerPhone;
            immobile.QuantityRooms = request.QuantityRooms;
            immobile.Price = request.Price;
            immobile.Cep = request.Cep;

            var adress = await cepClient.GetAdressAssync(request.Cep);

            immobile.Logradouro = adress.Logradouro;
            immobile.Complemento = adress.Complemento;
            immobile.Bairro = adress.Bairro;
            immobile.Localidade = adress.Localidade;
            immobile.Uf = adress.Uf;
            immobile.Unidade = adress.Unidade;
            immobile.Ibge = adress.Ibge;
            immobile.Gia = adress.Gia;

            AddNotifications(immobile);

            if (this.IsInvalid()) {
                return;
            }

            _repositoryImmobile.Update(immobile);
        }

    }
}

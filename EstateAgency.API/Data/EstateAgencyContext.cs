﻿using EstateAgency.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace EstateAgency.Data {
    public class EstateAgencyContext : DbContext {

        public DbSet<Immobile> Immobiles { get; set; }
        public DbSet<User> Users { get; set; }
        public EstateAgencyContext(DbContextOptions<EstateAgencyContext> options) : base(options) {
            Console.WriteLine("Contexto Criado");
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);
        }
    }
}

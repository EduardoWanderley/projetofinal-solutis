using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EstateAgency.Data;
using EstateAgency.Infra.Persistence.Repositories;
using EstateAgency.Interfaces.Repositories;
using EstateAgency.Interfaces.Services;
using EstateAgency.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace EstateAgency.API {
    public class Startup {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) {
            services.AddControllers();


            string connectionString = "Server=localhost;Database=projetosolutis;Trusted_Connection=True;";

            services.AddDbContext<EstateAgencyContext>((options) => options.UseSqlServer(connectionString));

            services.AddScoped(typeof(IRepositoryImmobile), typeof(RepositoryImmobile));
            services.AddScoped(typeof(IRepositoryUser), typeof(RepositoryUser));

            services.AddScoped(typeof(IServiceImmobile), typeof(ServiceImmobile));
            services.AddScoped(typeof(IServiceUser), typeof(ServiceUser));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }
}

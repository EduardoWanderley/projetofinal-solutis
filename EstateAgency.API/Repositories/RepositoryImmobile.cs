﻿using EstateAgency.Data;
using EstateAgency.Entities;
using EstateAgency.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EstateAgency.Infra.Persistence.Repositories {
    public class RepositoryImmobile : IRepositoryImmobile {

        protected readonly EstateAgencyContext _context;

        public RepositoryImmobile(EstateAgencyContext context) {
            _context = context;
        }

        public void Add(Immobile request) {
            _context.Immobiles.Add(request);
            _context.SaveChanges(); 
        }

        public void Delete(Immobile immobile) {
            _context.Immobiles.Remove(immobile);
            _context.SaveChanges();
        }

        public Immobile FindById(Guid id) {
            return _context.Immobiles.Where((p) => p.Id == id).SingleOrDefault();
        }

        public IEnumerable<Immobile> List() {
            return _context.Immobiles.ToList();
        }

        public IEnumerable<Immobile> ListByCep() {
            return _context.Immobiles.ToList();
        }

        public void Update(Immobile request) {
            _context.Update(request);
            _context.SaveChanges();
        }

    }
}

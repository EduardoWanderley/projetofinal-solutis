﻿using EstateAgency.Arguments.Immobile;
using EstateAgency.Entities;
using EstateAgency.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace EstateAgency.Controllers
{
    [Route("api/immobile")]
    [ApiController]
    public class ImmobileController : ControllerBase
    {

        private readonly IServiceImmobile _service;

        public ImmobileController(IServiceImmobile service)
        {
            _service = service;
        }

        /// <returns>Os imóveis cadastrados</returns>
        /// /// <response code="200">Success</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Business logic error, see return message for more info</response>
        /// <response code="401">Unauthorized. Token not present, invalid or expired</response>
        /// <response code="403">Forbidden. Resource access is denied</response>
        /// <response code="500">Due to server problems, it`s not possible to get your data now</response>
        /// <summary>
        /// Lista todos os imóveis do banco de dados.
        /// </summary>
        // GET: api/<Immobile>
        [Authorize] // qualquer user autenticado
        [HttpGet]
        public ActionResult<IEnumerable<Immobile>> Get()
        {

            var list = _service.List();

            return Ok(list);

        }


        /// <returns>Um imóvel através do Id</returns>
        /// <response code="200">Success</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Business logic error, see return message for more info</response>
        /// <response code="401">Unauthorized. Token not present, invalid or expired</response>
        /// <response code="403">Forbidden. Resource access is denied</response>
        /// <response code="500">Due to server problems, it`s not possible to get your data now</response>
        /// <summary>
        /// retorna um imóvel através do Id.
        /// </summary>
        // GET api/<Immobile>/5
        [Authorize(Roles = "PAYING_USER,MANAGER")] // usuarios pagantes e gerentes
        [HttpGet("{id}")]
        public ActionResult<Immobile> Get(Guid id)
        {

            var immobile = _service.FindById(id);

            if (immobile == null)
            {
                return NotFound();
            }

            return Ok(immobile);
        }

        /// <returns>Os imóveis cadastrados com base no Cep</returns>
        /// <response code="200">Success</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Business logic error, see return message for more info</response>
        /// <response code="401">Unauthorized. Token not present, invalid or expired</response>
        /// <response code="403">Forbidden. Resource access is denied</response>
        /// <response code="500">Due to server problems, it`s not possible to get your data now</response>
        /// <summary>
        /// Lista todos os imóveis através do cep.
        /// </summary>
        [Authorize(Roles = "PAYING_USER,MANAGER")] // usuarios pagantes e gerentes
        [HttpGet("getByCep/{cep}")]
        public ActionResult<IEnumerable<Immobile>> GetByCep(string cep)
        {

            var list = _service.ListByCep(cep);

            if (list == null)
            {
                return NotFound();
            }

            return Ok(list);

        }

        /// <returns>Cadastra um imóvel no banco.</returns>
        /// <response code="200">Success</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Business logic error, see return message for more info</response>
        /// <response code="401">Unauthorized. Token not present, invalid or expired</response>
        /// <response code="403">Forbidden. Resource access is denied</response>
        /// <response code="500">Due to server problems, it`s not possible to get your data now</response>
        /// <summary>
        /// Adiciona um novo imóvel.
        /// </summary>
        // POST api/<Immobile>
        [Authorize(Roles = "MANAGER")] // usuarios gerentes
        [HttpPost("add")]
        public async Task Post([FromBody] ImmobileRequest immobile)
        {
            if (immobile != null)
            {
                await _service.Add(immobile);
            }
            return;
        }

        /// <returns>Atualiza um imóvel no banco através do Id.</returns>
        /// <response code="200">Success</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Business logic error, see return message for more info</response>
        /// <response code="401">Unauthorized. Token not present, invalid or expired</response>
        /// <response code="403">Forbidden. Resource access is denied</response>
        /// <response code="500">Due to server problems, it`s not possible to get your data now</response>
        /// <summary>
        /// Atualiza um imóvel através do Id.
        /// </summary>
        [Authorize(Roles = "MANAGER")] // usuarios gerentes
        [HttpPut("update/{id}")]
        public async Task Update(Guid id, [FromBody] UpdateImmobileRequest immobile)
        {

            if (immobile != null)
            {
                await _service.Update(immobile, id);
            }
            return;
        }

        /// <returns>Deleta um imóvel no banco.</returns>
        /// <response code="200">Success</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Business logic error, see return message for more info</response>
        /// <response code="401">Unauthorized. Token not present, invalid or expired</response>
        /// <response code="403">Forbidden. Resource access is denied</response>
        /// <response code="500">Due to server problems, it`s not possible to get your data now</response>
        /// <summary>
        /// Deleta um imóvel através do Id.
        /// </summary>
        // DELETE api/<Immobile>/5
        [Authorize(Roles = "MANAGER")] // usuarios gerentes
        [HttpDelete("{id}")]
        public ActionResult Delete(Guid id)
        {
            _service.Delete(id);
            return Ok("Deleção realizada com sucesso!");
        }
    }
}
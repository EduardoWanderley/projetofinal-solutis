﻿using EstateAgency.Arguments.User;
using EstateAgency.Entities;
using EstateAgency.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace EstateAgency.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase {

        private readonly IServiceUser _service;

        public UserController(IServiceUser service) {
            _service = service;
        }

        /// <returns>Usuários cadastrados.</returns>
        /// <response code="200">Success</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Business logic error, see return message for more info</response>
        /// <response code="401">Unauthorized. Token not present, invalid or expired</response>
        /// <response code="403">Forbidden. Resource access is denied</response>
        /// <response code="500">Due to server problems, it`s not possible to get your data now</response>
        /// <summary>
        /// Lista todos os usuários do banco de dados.
        /// </summary>
        // GET: api/<UserController>
        [Authorize(Roles = "MANAGER")] // usuarios gerentes
        [HttpGet]
        public ActionResult<IEnumerable<User>> Get() {
           
            var list = _service.List();
            return Ok(list);

        }

        /// <returns>Um User através do Id</returns>
        /// <response code="200">Success</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Business logic error, see return message for more info</response>
        /// <response code="401">Unauthorized. Token not present, invalid or expired</response>
        /// <response code="403">Forbidden. Resource access is denied</response>
        /// <response code="500">Due to server problems, it`s not possible to get your data now</response>
        /// <summary>
        /// Retorna um usuário através do Id.
        /// </summary>
        // GET api/<UserController>/5
        [Authorize(Roles = "MANAGER")] // usuarios gerentes
        [HttpGet("{id}")]
        public ActionResult<User> Get(Guid id) {

            var user = _service.FindById(id);

            if (user == null) {
                return NotFound();
            }

            return Ok(user);
        }

        /// <returns>Cadastra um User no banco.</returns>
        /// <response code="200">Success</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Business logic error, see return message for more info</response>
        /// <response code="401">Unauthorized. Token not present, invalid or expired</response>
        /// <response code="403">Forbidden. Resource access is denied</response>
        /// <response code="500">Due to server problems, it`s not possible to get your data now</response>
        /// <summary>
        /// Adiciona um novo Usuário.
        /// </summary>
        // POST api/<UserController>
        [Authorize(Roles = "MANAGER")] // usuarios gerentes
        [HttpPost("add")]
        public UserResponse Post([FromBody] AddUserRequest user) {
            if (user != null) {
                 _service.Add(user);
            }

            var response = new UserResponse();
            response.UserName = user.UserName;
            response.TypeUser = user.TypeUser;

            return response;
        }

        /// <returns>Atualiza um User no banco através do Id.</returns>
        /// <response code="200">Success</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Business logic error, see return message for more info</response>
        /// <response code="401">Unauthorized. Token not present, invalid or expired</response>
        /// <response code="403">Forbidden. Resource access is denied</response>
        /// <response code="500">Due to server problems, it`s not possible to get your data now</response>
        /// <summary>
        /// Atualiza um usuário através do Id.
        /// </summary>
        // PUT api/<UserController>/update/5
        [Authorize(Roles = "MANAGER")] // usuarios gerentes
        [HttpPut("update/{id}")]
        public ActionResult Update(Guid id, [FromBody] UpdateUserRequest user) {

            if (user != null) {
                _service.Update(user);

                var response = new UserResponse();
                response.UserName = user.UserName;
                response.TypeUser = user.TypeUser;

                return Ok(response);
            }
            return NotFound();
        }

        /// <returns>Deleta um User no banco.</returns>
        /// <response code="200">Success</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Business logic error, see return message for more info</response>
        /// <response code="401">Unauthorized. Token not present, invalid or expired</response>
        /// <response code="403">Forbidden. Resource access is denied</response>
        /// <response code="500">Due to server problems, it`s not possible to get your data now</response>
        /// <summary>
        /// Deleta um usuário através do Id.
        /// </summary>
        // DELETE api/<UserController>/5
        [Authorize(Roles = "MANAGER")] // usuarios gerentes
        [HttpDelete("{id}")]
        public ActionResult Delete(Guid id) {
            _service.Delete(id);
            return Ok("Deleção realizada com sucesso!");
        }
    
    }
}

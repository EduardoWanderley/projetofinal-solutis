﻿using EstateAgency.Interfaces.Services;
using EstateAgency.Model.Arguments.User;
using EstateAgency.Security.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EstateAgency.Controllers {
    [Route("api/login")]
    [ApiController]
    public class LoginController : ControllerBase {
        
        private readonly IServiceUser _service;

        public LoginController(IServiceUser service) {
            _service = service;
        }

        /// <returns>Autenticação do usuário.</returns>
        /// <response code="200">Retorna o usuário autenticado.</response>
        /// <summary>
        /// método de autenticação de usuários
        /// </summary>
        [HttpPost("autenticar")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Authenticate([FromBody] AuthenticateUserRequest model) {
            try {
                var user = _service.Authenticate(model.UserName, model.Password);

                if (user == null)
                    return BadRequest(new { message = "Usuário ou senha inválidos" });

                var token = TokenService.GenerateToken(user);
                user.Password = "";

                return new {
                    user = user,
                    token = token
                };
            }
            catch (Exception ex) {
                return BadRequest(ex);
            }
        }

    }
}

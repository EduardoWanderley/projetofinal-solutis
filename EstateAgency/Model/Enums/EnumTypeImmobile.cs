﻿namespace EstateAgency.Enums
{
    public enum EnumTypeImmobile {
        HOUSE = 0,
        APARTMENT = 1, 
        MANSION = 2
    };
}
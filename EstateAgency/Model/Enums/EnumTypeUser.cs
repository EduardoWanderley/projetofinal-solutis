﻿namespace EstateAgency.Enums
{
    public enum EnumTypeUser {
        SAMPLE_USER = 0,
        PAYING_USER = 1,
        MANAGER = 2
    }
}
﻿using EstateAgency.Enums;
using EstateAgency.Model.Exceptions;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EstateAgency.Entities
{
    [Table("users")]
    public class User {
        
        [Column("Id")]
        public Guid Id { get; set; }
        [Column("user_name")]
        public string UserName { get; set; }
        [Column("password")]
        public string Password { get; set; }
        [Column("user_type")]
        public EnumTypeUser TypeUser { get; set; }

        public User(string userName, string password, EnumTypeUser typeUser) {
            
            Id = Guid.NewGuid();
            UserName = userName;
            Password = password;
            TypeUser = typeUser;
            
            if(userName.Length > 20) {
                throw new BusinessUserException("O nome de usuário deve conter no máximo 20 caracteres.");
            }

            if (password.Length > 12) {
                throw new BusinessUserException("A senha deve conter no máximo 12 caracteres.");
            }

        }

        public User() {
        }

    }
}
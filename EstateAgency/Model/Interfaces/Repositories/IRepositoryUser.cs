﻿using EstateAgency.Entities;
using System;
using System.Collections.Generic;

namespace EstateAgency.Interfaces.Repositories
{
    public interface IRepositoryUser {

        public User GetUserAuthenticate(string name, string password);

        void Add(User request);

        void Update(User request);

        IEnumerable<User> List();

        User FindById(Guid id);

        void Delete(User user);
    }
}

﻿using EstateAgency.Entities;
using Refit;
using System.Threading.Tasks;

namespace EstateAgency.Interfaces.Services
{
    public interface ICepApiService {

        [Get("/ws/{cep}/json")]
        Task<Immobile> GetAdressAssync(string cep);
    }
}

﻿using EstateAgency.Model.Exceptions;

namespace EstateAgency.Arguments.Immobile
{
    public class ImmobileRequest {
        public ImmobileRequest(
            int typeImmobile, int quantityRooms, decimal price, long ownerPhone, string cep) 
            {
            TypeImmobile = typeImmobile;
            QuantityRooms = quantityRooms;
            Price = price;
            OwnerPhone = ownerPhone;
            Cep = cep;

            if (typeImmobile < 0 || typeImmobile > 2)
            {
                throw new BusinessImmobileException("Digite um tipo de imóvel válido!");
            }

        }

        public ImmobileRequest() {       
        }

        public int TypeImmobile { get; set; }
        public int QuantityRooms { get; set; }
        public decimal Price { get; set; }
        public long OwnerPhone { get; set; }
        public string Cep { get; set; }
    }
}
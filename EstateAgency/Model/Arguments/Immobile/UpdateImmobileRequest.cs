﻿namespace EstateAgency.Arguments.Immobile
{
    public class UpdateImmobileRequest {
        public UpdateImmobileRequest(int typeImmobile, int quantityRooms, decimal price, long ownerPhone, string cep) {
            TypeImmobile = typeImmobile;
            QuantityRooms = quantityRooms;
            Price = price;
            OwnerPhone = ownerPhone;
            Cep = cep;
        }

        public UpdateImmobileRequest() {
        }

        public int TypeImmobile { get; set; }
        public int QuantityRooms { get; set; }
        public decimal Price { get; set; }
        public long OwnerPhone { get; set; }
        public string Cep { get; set; }

    }
}

﻿namespace EstateAgency.Arguments.User
{
    public class AddUserRequest {
        public AddUserRequest(string userName, string password, int typeUser) {
            UserName = userName;
            Password = password;
            TypeUser = typeUser;
        }
        public AddUserRequest() {
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public int TypeUser { get; set; }

    }
}

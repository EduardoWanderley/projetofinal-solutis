﻿namespace EstateAgency.Model.Arguments.User
{
    public class AuthenticateUserRequest {
        public AuthenticateUserRequest(string userName, string password) {
            UserName = userName;
            Password = password;
        }
        public AuthenticateUserRequest() {   
        }

        public string UserName { get; set; }
        public string Password { get; set; }

    }
}

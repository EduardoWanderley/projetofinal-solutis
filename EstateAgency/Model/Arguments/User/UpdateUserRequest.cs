﻿using System;

namespace EstateAgency.Arguments.User
{
    public class UpdateUserRequest {
        public UpdateUserRequest( string userName, string password, int typeUser) {
            UserName = userName;
            Password = password;
            TypeUser = typeUser;
        }

        public UpdateUserRequest() {
        }

        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int TypeUser { get; set; }
 
    }
}

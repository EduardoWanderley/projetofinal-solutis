﻿using System;

namespace EstateAgency.Arguments.User
{
    public class UserResponse {
        public UserResponse() {
        }

        public UserResponse(Guid id, string userName, int typeUser) {
            Id = id;
            UserName = userName;
            TypeUser = typeUser;
        }

        public Guid Id { get; set; }
        public string UserName { get; set; }
        public int TypeUser { get; set; }

        public static explicit operator UserResponse(Entities.User entidade) {
            return new UserResponse() {
                Id = entidade.Id,
                UserName = entidade.UserName,
                TypeUser = (int) entidade.TypeUser
            };
        }

        public override string ToString() {
            return $"Id : {Id}\nNome de usuário : {UserName}\nTipo de usuário : {TypeUser.ToString()}";
        }
    }
}

﻿using System;
using System.Runtime.Serialization;

namespace EstateAgency.Model.Exceptions
{

    [Serializable]
    public class BusinessImmobileException : Exception {
        public BusinessImmobileException() {
        }

        public BusinessImmobileException(string message) : base(message) {
        }

        public BusinessImmobileException(string message, Exception innerException) : base(message, innerException) {
        }

        protected BusinessImmobileException(SerializationInfo info, StreamingContext context) : base(info, context) {
        }



    }
}

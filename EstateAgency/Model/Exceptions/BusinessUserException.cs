﻿using System;
using System.Runtime.Serialization;

namespace EstateAgency.Model.Exceptions
{

    [Serializable]
    public class BusinessUserException : Exception{

        public BusinessUserException() {
        }

        public BusinessUserException(string message) : base(message) {
        }

        public BusinessUserException(string message, Exception innerException) : base(message, innerException) {
        }

        protected BusinessUserException(SerializationInfo info, StreamingContext context) : base(info, context) {
        }

    }
}

﻿using EstateAgency.Arguments.User;
using EstateAgency.Entities;
using EstateAgency.Enums;
using EstateAgency.Interfaces.Repositories;
using EstateAgency.Interfaces.Services;
using EstateAgency.Model.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EstateAgency.Services
{
    public class ServiceUser : IServiceUser {

        private readonly IRepositoryUser _repositoryUser;

        public ServiceUser() {
        }

        public ServiceUser(IRepositoryUser repositoryUser) {
            _repositoryUser = repositoryUser;
        }
        
        public void Add(AddUserRequest request) {
            
            if(request == null) {
                throw new BusinessUserException("Os dados do usuário não podemn ser nulos.");
            }

            EnumTypeUser type = (EnumTypeUser)request.TypeUser;

            User user = new User(request.UserName,request.Password,type);

            _repositoryUser.Add(user);
        }

        public void Delete(Guid id) {
            var user = _repositoryUser.FindById(id);

            if (user == null) {
                throw new BusinessUserException("Usuário não encontrado!");
            }

            _repositoryUser.Delete(user);
        }

        public UserResponse FindById(Guid id) {
            
            var user = _repositoryUser.FindById(id);

            if(user == null) {
                throw new BusinessUserException("Usuário não encontrado!");
            }

            return (UserResponse)user;
        }

        public IEnumerable<UserResponse> List() {
            var list =  _repositoryUser.List().ToList().Select(user => (UserResponse) user).ToList();

            if(list == null) {
                throw new BusinessUserException("Lista Vazia!");
            }

            return list;

        }

        public void Update(UpdateUserRequest request) {
            var user = _repositoryUser.FindById(request.Id);

            if(user == null) {
                throw new BusinessUserException("Usuário não encontrado!");
            }

            EnumTypeUser type = (EnumTypeUser)request.TypeUser;

            user.UserName = request.UserName;
            user.Password = request.Password;
            user.TypeUser = type;

            _repositoryUser.Update(user);
        }

        public User Authenticate(string nome, string password) {

            var user = _repositoryUser.GetUserAuthenticate(nome, password);

            if (user == null)
                return null;

            return user;
        }

    }
}

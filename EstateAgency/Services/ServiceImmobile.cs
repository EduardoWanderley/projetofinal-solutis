﻿using EstateAgency.Arguments.Immobile;
using EstateAgency.Entities;
using EstateAgency.Enums;
using EstateAgency.Interfaces.Repositories;
using EstateAgency.Interfaces.Services;
using EstateAgency.Model.Exceptions;
using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EstateAgency.Services
{
    public class ServiceImmobile : IServiceImmobile {

        private readonly IRepositoryImmobile _repositoryImmobile;
        public ServiceImmobile() {
        }
        public ServiceImmobile(IRepositoryImmobile repository) {
            _repositoryImmobile = repository;
        }

        public async Task Add(ImmobileRequest request) {

            if (request == null) {
                throw new BusinessImmobileException("Por favor, preencha todos os campos necessários.");
            }

            var cepClient = RestService.For<ICepApiService>("http://viacep.com.br");

            EnumTypeImmobile type = (EnumTypeImmobile)request.TypeImmobile;

            Immobile immobile = new Immobile(type, request.QuantityRooms, request.Price, request.OwnerPhone, request.Cep); 

            var adress = await cepClient.GetAdressAssync(immobile.Cep);

            immobile.Logradouro = adress.Logradouro;
            immobile.Complemento = adress.Complemento;
            immobile.Bairro = adress.Bairro;
            immobile.Localidade = adress.Localidade;
            immobile.Uf = adress.Uf;
            immobile.Unidade = adress.Unidade;
            immobile.Ibge = adress.Ibge;
            immobile.Gia = adress.Gia;

            if(immobile != null) {
                _repositoryImmobile.Add(immobile);
            }

        }

        public ImmobileResponse Delete(Guid id) {
            var immobile = _repositoryImmobile.FindById(id);

            if (immobile == null) {
                throw new BusinessImmobileException("Imóvel não encontrado!");
            }

            _repositoryImmobile.Delete(immobile);

            return (ImmobileResponse)immobile;
        }

        public Immobile FindById(Guid id) {

            var immobile = _repositoryImmobile.FindById(id);

            if (immobile == null) {
                throw new BusinessImmobileException("Imóvel não encontrado!");
            }

            return immobile;
        }

        public IEnumerable<Immobile> List() {

            var list = _repositoryImmobile.List().ToList();
        
            if(list == null) {
                throw new BusinessImmobileException("Lista vazia!");
            }

            return list;
        }

        public IEnumerable<Immobile> ListByCep(string cep) {
            var list =  _repositoryImmobile.ListByCep().ToList().Where(i => i.Cep == cep).ToList();

            if (list == null) {
                throw new BusinessImmobileException("Lista vazia!");
            }

            return list;
        }

        public async Task Update(UpdateImmobileRequest request, Guid id) {

            if (request == null) {
                throw new BusinessImmobileException("Por favor, preencha todos os campos necessários.");
            }

            Immobile immobile = _repositoryImmobile.FindById(id);

            var cepClient = RestService.For<ICepApiService>("http://viacep.com.br");

            EnumTypeImmobile type = (EnumTypeImmobile)request.TypeImmobile;

            immobile.Type = type;
            immobile.OwnerPhone = request.OwnerPhone;
            immobile.QuantityRooms = request.QuantityRooms;
            immobile.Price = request.Price;
            immobile.Cep = request.Cep;

            var adress = await cepClient.GetAdressAssync(request.Cep);

            immobile.Logradouro = adress.Logradouro;
            immobile.Complemento = adress.Complemento;
            immobile.Bairro = adress.Bairro;
            immobile.Localidade = adress.Localidade;
            immobile.Uf = adress.Uf;
            immobile.Unidade = adress.Unidade;
            immobile.Ibge = adress.Ibge;
            immobile.Gia = adress.Gia;

            _repositoryImmobile.Update(immobile);

        }

    }
}

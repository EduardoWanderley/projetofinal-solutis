﻿using EstateAgency.Data;
using EstateAgency.Entities;
using EstateAgency.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EstateAgency.Infra.Persistence.Repositories
{
    public class RepositoryUser : IRepositoryUser {

        protected readonly EstateAgencyContext _context;

        public RepositoryUser(EstateAgencyContext context) {
            _context = context;
        }
        
        public void Add(User request) {
            _context.Users.Add(request);
            _context.SaveChanges();
        }

        public void Delete(User user) {
            _context.Users.Remove(user);
            _context.SaveChanges();
        }

        public User FindById(Guid id) {
            return _context.Users.Find(id);
        }

        public IEnumerable<User> List() {
            return _context.Users.ToList();
        }

        public void Update(User request) {
            _context.Update(request);
            _context.SaveChanges();
        }

        //autenticação
        public User GetUserAuthenticate(string name, string password) {
            return _context.Users.ToList().Where(u => u.UserName.ToLower() == name.ToLower() && u.Password.ToLower() == password.ToLower()).SingleOrDefault();
        }
    }

}

﻿using EstateAgency.Entities;
using Microsoft.EntityFrameworkCore;

namespace EstateAgency.Data
{
    public class EstateAgencyContext : DbContext {

        public DbSet<Immobile> Immobiles { get; set; }
        public DbSet<User> Users { get; set; }
        public EstateAgencyContext(DbContextOptions<EstateAgencyContext> options) : base(options) { 
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);
        }
    }
}

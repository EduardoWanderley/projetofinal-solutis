# Estate Agency

* # Descrição : 
    Projeto que simula um sistema para um site de venda de imóveis, onde o usuário pode fazer requisições com base na sua autenticação.

    Dentro da api, existem 4 tipos de usuários :

     * usuário autenticado [não pagante]
     * usuário autenticado [pagante]
     * usuário autenticado [gerente]
     * Usuário não autenticado;



* # Tipos de requisições :
    
    * Requisição de Login [Qualquer usuário]
    
    * Listar todos os imóveis [Qualquer usuário que esteja autenticado]
    * Retornar imóvel com base no Id [Usuários gerentes e pagantes]
    * Listar imóveis com base no Cep [Usuários gerentes e pagantes]   
    * Adicionar imóvel [Usuário Gerente]
    * Atualizar imóvel [Usuário Gerente]
    * Remover imóvel [Usuário Gerente]



* # Banco de dados :

    Microsoft SQL Server (www.microsoft.com/pt-br/sql-server/sql-server-downloads)

* # Documentação da API :

    Swagger (www.swagger.io)

* # API externa utilizada : 

    ViaCep (www.viacep.com.br)




